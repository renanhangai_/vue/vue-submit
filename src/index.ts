import { SubmitManager } from "./SubmitManager";
import { VueSubmitPlugin } from "./Plugin";
export { AxiosDownload } from './util/Download';

export { SubmitManager };
export default VueSubmitPlugin;