# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="2.3.0"></a>
# [2.3.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.2.1...v2.3.0) (2020-02-06)


### Features

* Added axios option on request to allow the use of another axios instance ([bc62433](https://gitlab.com/renanhangai_/vue/vue-submit/commit/bc62433))



<a name="2.2.1"></a>
## [2.2.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.2.0...v2.2.1) (2019-09-09)


### Bug Fixes

* new Buefy toast as dialog options ([4700287](https://gitlab.com/renanhangai_/vue/vue-submit/commit/4700287))



<a name="2.2.0"></a>
# [2.2.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.1.2...v2.2.0) (2019-08-20)


### Bug Fixes

* Type of notifyError ([8a95041](https://gitlab.com/renanhangai_/vue/vue-submit/commit/8a95041))


### Features

* Added async on notify and notifyError ([799f372](https://gitlab.com/renanhangai_/vue/vue-submit/commit/799f372))
* Added new error context on notifyError ([c413489](https://gitlab.com/renanhangai_/vue/vue-submit/commit/c413489))



<a name="2.1.2"></a>
## [2.1.2](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.1.1...v2.1.2) (2019-06-13)



<a name="2.1.1"></a>
## [2.1.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.1.0...v2.1.1) (2019-06-13)


### Bug Fixes

* Validator when function was not being called on the vm ([4d1c3bb](https://gitlab.com/renanhangai_/vue/vue-submit/commit/4d1c3bb))



<a name="2.1.0"></a>
# [2.1.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.0.3...v2.1.0) (2019-06-13)


### Features

* $submit now accepts validator as an array ([3ce20a9](https://gitlab.com/renanhangai_/vue/vue-submit/commit/3ce20a9))



<a name="2.0.3"></a>
## [2.0.3](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.0.2...v2.0.3) (2019-06-08)


### Bug Fixes

* Notify error was not being called ([8e21ac7](https://gitlab.com/renanhangai_/vue/vue-submit/commit/8e21ac7))



<a name="2.0.2"></a>
## [2.0.2](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.0.1...v2.0.2) (2019-05-02)



<a name="2.0.1"></a>
## [2.0.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v2.0.0...v2.0.1) (2019-04-25)



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.5.0...v2.0.0) (2019-04-25)



<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.4.1...v1.5.0) (2019-04-05)


### Features

* Added setup method for the submit. ([a075f81](https://gitlab.com/renanhangai_/vue/vue-submit/commit/a075f81))



<a name="1.4.1"></a>
## [1.4.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.4.0...v1.4.1) (2019-03-16)


### Bug Fixes

* Default error handler changed to console.error ([1c3f7bf](https://gitlab.com/renanhangai_/vue/vue-submit/commit/1c3f7bf))



<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.3.3...v1.4.0) (2019-03-11)


### Features

* Added errorHandler and fixed default error handler on vue-submit ([8dbe2f3](https://gitlab.com/renanhangai_/vue/vue-submit/commit/8dbe2f3))



<a name="1.3.3"></a>
## [1.3.3](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.3.2...v1.3.3) (2019-02-25)



<a name="1.3.2"></a>
## [1.3.2](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.3.0...v1.3.2) (2019-02-22)


### Bug Fixes

* Do not notify sentry when there is no exception ([d6b4031](https://gitlab.com/renanhangai_/vue/vue-submit/commit/d6b4031))
* Success callback was not being called with result ([f9b4933](https://gitlab.com/renanhangai_/vue/vue-submit/commit/f9b4933))



<a name="1.3.1"></a>
## [1.3.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.3.0...v1.3.1) (2019-02-21)


### Bug Fixes

* Do not notify sentry when there is no exception ([d6b4031](https://gitlab.com/renanhangai_/vue/vue-submit/commit/d6b4031))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.2.0...v1.3.0) (2019-02-20)


### Features

* Added AxiosDownload export ([770b024](https://gitlab.com/renanhangai_/vue/vue-submit/commit/770b024))
* Added sentry support ([8d3376c](https://gitlab.com/renanhangai_/vue/vue-submit/commit/8d3376c))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.1.4...v1.2.0) (2019-02-19)


### Features

* New notifyError function option ([4470af7](https://gitlab.com/renanhangai_/vue/vue-submit/commit/4470af7))



<a name="1.1.4"></a>
## [1.1.4](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.1.3...v1.1.4) (2019-02-06)



<a name="1.1.3"></a>
## [1.1.3](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.1.2...v1.1.3) (2019-01-18)


### Bug Fixes

* Removed console.log ([d300ec1](https://gitlab.com/renanhangai_/vue/vue-submit/commit/d300ec1))



<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.1.0...v1.1.2) (2019-01-18)


### Bug Fixes

* Problems with validation error and instanceof ([e9e2357](https://gitlab.com/renanhangai_/vue/vue-submit/commit/e9e2357))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.1.0...v1.1.1) (2018-12-05)


### Bug Fixes

* Problems with validation error and instanceof ([e9e2357](https://gitlab.com/renanhangai_/vue/vue-submit/commit/e9e2357))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.8...v1.1.0) (2018-12-04)



<a name="1.0.8"></a>
## [1.0.8](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.7...v1.0.8) (2018-12-03)


### Bug Fixes

* Typo on notifyDefaultsError ([10d19c1](https://gitlab.com/renanhangai_/vue/vue-submit/commit/10d19c1))



<a name="1.0.7"></a>
## [1.0.7](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.6...v1.0.7) (2018-12-02)


### Bug Fixes

* Error.ts was using .js ([fdd4268](https://gitlab.com/renanhangai_/vue/vue-submit/commit/fdd4268))



<a name="1.0.6"></a>
## [1.0.6](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.5...v1.0.6) (2018-11-29)


### Bug Fixes

* Submission and notification when confirmation is skipped ([a305495](https://gitlab.com/renanhangai_/vue/vue-submit/commit/a305495))



<a name="1.0.5"></a>
## [1.0.5](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.4...v1.0.5) (2018-11-29)



<a name="1.0.4"></a>
## [1.0.4](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.3...v1.0.4) (2018-11-27)



<a name="1.0.3"></a>
## [1.0.3](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.2...v1.0.3) (2018-11-27)


### Features

* Fixed module resolve on nuxt ([0bba1f3](https://gitlab.com/renanhangai_/vue/vue-submit/commit/0bba1f3))



<a name="1.0.2"></a>
## [1.0.2](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.1...v1.0.2) (2018-11-27)


### Bug Fixes

* Missing path import ([c4dc18d](https://gitlab.com/renanhangai_/vue/vue-submit/commit/c4dc18d))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v1.0.0...v1.0.1) (2018-11-27)


### Bug Fixes

* Npmignore on nuxt files ([d69d5f1](https://gitlab.com/renanhangai_/vue/vue-submit/commit/d69d5f1))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v0.0.2...v1.0.0) (2018-11-27)



<a name="0.0.2"></a>
## [0.0.2](https://gitlab.com/renanhangai_/vue/vue-submit/compare/v0.0.1...v0.0.2) (2018-11-27)


### Bug Fixes

* $submitting prop was not being set ([d78acfd](https://gitlab.com/renanhangai_/vue/vue-submit/commit/d78acfd))



<a name="0.0.1"></a>
## 0.0.1 (2018-11-27)
